package com.androidphotos10;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import static com.androidphotos10.AlbumScreen.currPhoto;

public class PhotoScreen extends Activity {
    private ImageView imageView;
    private TextView tagname;
    private TextView tagvalue;

    ListView listView;
    ArrayAdapter<Tag> arrayAdapter;
    int albumind = MainActivity.albums.indexOf(MainActivity.currAlbum);
    int photoind = MainActivity.albums.get(albumind).photos.indexOf(currPhoto);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_screen);

        imageView = (ImageView)findViewById(R.id.imageView3);
        tagname = findViewById(R.id.tagname);
        tagvalue = findViewById(R.id.tagvalue);
        listView = findViewById(R.id.taglist);

        //test
//        MainActivity.currAlbum = MainActivity.albums.get(0);
//        AlbumScreen.currPhoto = MainActivity.albums.get(0).photos.get(0);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        final Photo photo = (Photo) bundle.getSerializable("photo");

        ArrayList<Tag> tags = photo.getTags();

        arrayAdapter = new ArrayAdapter<Tag>(
                this,
                android.R.layout.simple_list_item_1,
                tags);

        listView.setAdapter(arrayAdapter);

        imageView.setImageURI(Uri.fromFile(photo.getfp()));

        final Button delete = findViewById(R.id.deletetag);
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                MainActivity.albums.get(albumind).photos.get(photoind).tags.remove(new Tag(tagname.getText().toString(),tagvalue.getText().toString()));

                try {
                    FileOutputStream fos = PhotoScreen.this.openFileOutput("albums.ser", Context.MODE_PRIVATE);
                    ObjectOutputStream os = new ObjectOutputStream(fos);
                    os.writeObject(MainActivity.albums);
                    os.close();
                    fos.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        final Button add = findViewById(R.id.addtag);
        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String name = tagname.getText().toString();
                String value = tagvalue.getText().toString();
                AlbumScreen.currPhoto.addTag(new Tag(name,value));

                try {
                    FileOutputStream fos = PhotoScreen.this.openFileOutput("albums.ser", Context.MODE_PRIVATE);
                    ObjectOutputStream os = new ObjectOutputStream(fos);
                    os.writeObject(MainActivity.albums);
                    os.close();
                    fos.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        final Button move = findViewById(R.id.movephoto);
        move.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        PhotoScreen.this);
                alert.setTitle("Move to");

                final EditText input = new EditText(PhotoScreen.this);
                alert.setView(input);


                input.selectAll();

                alert.setPositiveButton("MOVE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String srt1 = input.getEditableText().toString();
                        //update your listview here

                        MainActivity.albums.get(MainActivity.albums.indexOf(srt1)).photos.add(currPhoto);
                        MainActivity.albums.get(albumind).photos.remove(currPhoto);

                        try {
                            FileOutputStream fos = PhotoScreen.this.openFileOutput("albums.ser", Context.MODE_PRIVATE);
                            ObjectOutputStream os = new ObjectOutputStream(fos);
                            os.writeObject(MainActivity.albums);
                            os.close();
                            fos.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });

        final Button prev = findViewById(R.id.prev);
        prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                photoind--;
                if(photoind >= 0){
                    AlbumScreen.currPhoto = MainActivity.albums.get(albumind).photos.get(photoind);
                    setContentView(R.layout.activity_photo_screen);
                }
            }
        });

        final Button next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                photoind++;
                if(photoind < MainActivity.currAlbum.getSize()) {
                    AlbumScreen.currPhoto = MainActivity.albums.get(albumind).photos.get(photoind);
                    setContentView(R.layout.activity_photo_screen);
                }
            }
        });
    }
}
