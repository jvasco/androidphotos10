package com.androidphotos10;
/**
 * @author Nicholas Lelchitsky
 * @author Jordy Vasco
 */


import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class com.androidphotos10.Tag.
 */
public class Tag implements Serializable{

    /** The tag. */
    String[]tag = new String[2];

    /**
     * Instantiates a new tag.
     *
     * @param name the name
     * @param value the value
     */
    public Tag(String name, String value){
        this.tag[0] = name;
        this.tag[1] = value;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName(){
        return tag[0];
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue(){
        return tag[1];
    }

    /**
     * Sets the tag.
     *
     * @param name the name
     * @param value the value
     */
    public void setTag(String name, String value){
        this.tag[0] = name;
        this.tag[1] = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
            result = false;
        } else {
            Tag t = (Tag) object;
            if (this.tag[0].toLowerCase().equals(t.tag[0].toLowerCase()) && this.tag[1].toLowerCase().equals(t.tag[1].toLowerCase())) {
                result = true;
            }
        }
        return result;
    }

    public String toString(){
        return (tag[0] + ": " + tag[1]);
    }

}
