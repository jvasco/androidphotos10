package com.androidphotos10;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class SearchActivity extends Activity implements SearchView.OnQueryTextListener{
    ListView resultList;
    EditText editText;
    SearchView searchView;
    ArrayList<Photo> photoList = new ArrayList<Photo>();
    ArrayAdapter<Photo> arrayAdapter;
    PhotoListAdapter photoListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        resultList = (ListView)findViewById(R.id.resultList);
        resultList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg) {
                Intent appInfo = new Intent(SearchActivity.this, PhotoScreen.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("photo", (Serializable) resultList.getItemAtPosition(position));
                AlbumScreen.currPhoto = (Photo)resultList.getItemAtPosition(position);
                appInfo.putExtras(bundle);
                startActivity(appInfo);
            }
        });
       searchView = (SearchView)findViewById(R.id.search);
       initList();
      searchView.setOnQueryTextListener((SearchView.OnQueryTextListener) this);
    //this.setListAdapter()
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        String text = newText;
            if(text.equals("")) {
            initList();
        }
    else {
                ArrayList<Photo> temp = new ArrayList<Photo>();
                temp = (ArrayList) photoList.clone();
                photoList.clear();
                Iterator<Photo> photoIter = temp.iterator();

                while (photoIter.hasNext()) {
                    System.out.println("Something");
                    Photo photo = photoIter.next();
                    System.out.println("Caption: " + photo.getCaption());
                    for(Tag tag: photo.getTags()){
                        System.out.println("Tag is: " + tag.toString());
                        if(tag.getValue().toLowerCase().contains(text.toLowerCase())){
                            photoList.add(photo);
                            photoListAdapter.notifyDataSetChanged();
                        }
                    }
                    //if (!photo.getCaption().toLowerCase().contains(text.toLowerCase())) {
                }
                photoListAdapter.notifyDataSetChanged();
            }
        return false;
    }

    private void initList() {
        photoList.clear();
        for(Album album: MainActivity.albums){
            for(Photo photo: album.getPhotos()){
                System.out.println("Photo being added");
                photoList.add(photo);
            }
        }
        arrayAdapter = new ArrayAdapter<Photo>(this, android.R.layout.simple_list_item_1,
                photoList);
        photoListAdapter = new PhotoListAdapter(this, photoList);
        resultList.setAdapter(photoListAdapter);
    }

    public void searchItem(String text){
        for(Photo photo: photoList){
            if(photo.tags.contains(text))
                return;

        }
        return;
    }

}
