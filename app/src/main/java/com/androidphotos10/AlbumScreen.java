package com.androidphotos10;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import static com.androidphotos10.MainActivity.albums;

public class AlbumScreen extends AppCompatActivity {

    PhotoListAdapter photoListAdapter;
    private final int SELECT_PHOTO = 1;
    private ImageView imageView;
    public static Photo currPhoto;
  //  int albumind = MainActivity.albums.indexOf(MainActivity.currAlbum);
//    int photoind = MainActivity.albums.get(albumind).photos.indexOf(AlbumScreen.currPhoto);

    ListView photosList;
    ArrayList<Photo> photoList = new ArrayList<Photo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_screen);
        photosList = (ListView)findViewById(R.id.photosList);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        Album album = (Album) bundle.getSerializable("album");

        System.out.println("Album received is: " + album.getName());
        TextView albumName = (TextView)findViewById(R.id.albumName);
        System.out.println("PHotos iin album " + album.getName() + " are: " + album.getPhotos().toString());
        albumName.setText(album.getName());
        for(Photo photo: album.getPhotos()){
            photoList.add(photo);
        }

        photoListAdapter = new PhotoListAdapter(this, photoList);
        photosList.setAdapter(photoListAdapter);
        registerForContextMenu(photosList);
        photosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg) {
                Intent appInfo = new Intent(AlbumScreen.this, PhotoScreen.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("photo", (Serializable) photosList.getItemAtPosition(position));
                currPhoto = (Photo)photosList.getItemAtPosition(position);
                appInfo.putExtras(bundle);

                startActivity(appInfo);
            }
        });
       // imageView = (ImageView)findViewById(R.id.imageView);

        Button add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });

/*
        final Button delete = findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
            }
        });

        final Button display = findViewById(R.id.display);
        display.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setContentView(R.layout.activity_photo_screen);
            }
        });
*/
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        System.out.println("Yawk");
        if (v.getId() == R.id.photosList) {
            System.out.println("Sheesh");
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle("Options");
            menu.add(Menu.NONE, 0, 0, "Open");
            menu.add(Menu.NONE, 1, 1, "Delete");
        }
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {

        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int menuItemIndex = item.getItemId();
        if (menuItemIndex == 0) {
            startActivity(new Intent(AlbumScreen.this, PhotoScreen.class));
        }

        if (menuItemIndex == 1) {
            //delete photo
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm");
            builder.setMessage("Are you sure you want to delete this album?");
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Delete photo
                    photoListAdapter.remove(photoListAdapter.getItem(info.position));
                    MainActivity.albums.get(MainActivity.albums.indexOf(MainActivity.currAlbum)).photos.remove(photoListAdapter.getItem(info.position));

                    try {
                        FileOutputStream fos = AlbumScreen.this.openFileOutput("albums.ser", Context.MODE_PRIVATE);
                        ObjectOutputStream os = new ObjectOutputStream(fos);
                        os.writeObject(MainActivity.albums);
                        os.close();
                        fos.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    try {
                        final Uri imageUri = imageReturnedIntent.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        //imageView.setImageBitmap(selectedImage);
                        //MainActivity.albums.get(albums.indexOf(MainActivity.currAlbum)).addPhoto(new Photo(new File(imageUri.getPath())));
                        MainActivity.albums.get(0).addPhoto(new Photo(new File(imageUri.getPath())));
                        photoList.add(new Photo(new File(imageUri.getPath())));
                        photoListAdapter.notifyDataSetChanged();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    try {
                        FileOutputStream fos = this.openFileOutput("albums.ser", Context.MODE_PRIVATE);
                        ObjectOutputStream os = new ObjectOutputStream(fos);
                        os.writeObject(MainActivity.albums);
                        os.close();
                        fos.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
