package com.androidphotos10;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.io.File;
import java.util.List;

public class PhotoListAdapter extends ArrayAdapter<Photo>{

    List<Photo> photoList;

    public PhotoListAdapter(@NonNull Context context, List<Photo> tempList) {
        super(context, R.layout.mylist, tempList);
    photoList = tempList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        convertView = new LinearLayout(getContext());
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
        convertView = vi.inflate(R.layout.mylist, parent, false);
        Photo photo = getItem(position);

        TextView txtTitle = (TextView) convertView.findViewById(R.id.caption);
        txtTitle.setText(photo.fp.getPath().substring(photo.fp.getPath().lastIndexOf('/') + 1));

        ImageView img = (ImageView)convertView.findViewById(R.id.thumbnail);
        //img.setImageResource(R.drawable.stock2);
        img.setImageURI(Uri.fromFile(photo.getfp()));
        /*
        File imageFile = photo.getfp();
        System.out.println("File path is: " + imageFile.getAbsolutePath().toString());

        if(imageFile.exists()) {

            System.out.println("Its alive!");
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath().toString());
            img.setImageBitmap(bitmap);

        }
        */
        return convertView;
    }
}
