package com.androidphotos10;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    public static ArrayList<Album> albums = new ArrayList<Album>();
    ListView listView;
    ArrayAdapter<Album> arrayAdapter;
    PhotoListAdapter photoListAdapter;
    public static Album currAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        System.out.println("Test");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.albumlist);
        Album s = new Album("stock");

        Bitmap bm1 = BitmapFactory.decodeResource(this.getResources(), R.drawable.stock1);
        Bitmap bm2 = BitmapFactory.decodeResource(this.getResources(), R.drawable.stock2);
        Bitmap bm3 = BitmapFactory.decodeResource(this.getResources(), R.drawable.stock3);

        Photo s1 = new Photo(bitmapToFile(bm1,1));
        Photo s2 = new Photo(bitmapToFile(bm2,2));
        Photo s3 = new Photo(bitmapToFile(bm3,3));

        s1.caption = "Hello";
        s2.caption = "World";
        s3.caption = "Test";

        s1.addTag(new Tag("Name", "John"));
        s2.addTag(new Tag("Name", "Joe"));
        s3.addTag(new Tag("Location", "April"));
        s1.addTag(new Tag("Name", "zyx"));

        s.addPhoto(s1);
        s.addPhoto(s2);
        s.addPhoto(s3);

        albums.add(s);
        //albums.add(new Album("Stuff"));
        //albums.add(new Album("TESTING"));

        String fileName = "albums.ser";

//        try {
//            FileOutputStream fos = this.openFileOutput(fileName, Context.MODE_PRIVATE);
//            ObjectOutputStream os = new ObjectOutputStream(fos);
//            os.writeObject(albums);
//            os.close();
//            fos.close();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }

        try {
            FileInputStream fis = this.openFileInput(fileName);
            ObjectInputStream is = new ObjectInputStream(fis);
            albums = (ArrayList) is.readObject();
            is.close();
            fis.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //System.out.println(albums.get(0).photos.get(0).fp.toString());

        arrayAdapter = new ArrayAdapter<Album>(
                this,
                android.R.layout.simple_list_item_1,
                albums);

        ArrayList<Photo> photoList = new ArrayList<Photo>();
        for (Album album : albums) {
            for (Photo photo : album.getPhotos()) {
                photoList.add(photo);
            }
        }
        photoListAdapter = new PhotoListAdapter(this, photoList);


        listView.setAdapter(arrayAdapter);


        /*
        final Button add = findViewById(R.id.open);
        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                setContentView(R.layout.activity_album_screen);
            }
        });
        */
        //find a way to get list of album names into listview

        final Button search = findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                System.out.println("SEARCH PRESSED");
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
            }
        });

        final Button newAlbum = findViewById(R.id.newAlbum);
        newAlbum.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        MainActivity.this);
                alert.setTitle("New Album");

                final EditText input = new EditText(MainActivity.this);
                alert.setView(input);


                input.selectAll();

                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String srt1 = input.getEditableText().toString();
                        //update your listview here
                        arrayAdapter.add(new Album(srt1));
                        albums.add(new Album(srt1));
                        try {
                            FileOutputStream fos = MainActivity.this.openFileOutput("albums.ser", Context.MODE_PRIVATE);
                            ObjectOutputStream os = new ObjectOutputStream(fos);
                            os.writeObject(albums);
                            os.close();
                            fos.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }

        });

//        FileInputStream fis = null;
//        ObjectInputStream in = null;
//        ArrayList albums = new ArrayList();
//        try {
//            fis = new FileInputStream("file.ser");
//            in = new ObjectInputStream(fis);
//            albums = (ArrayList) in.readObject();
//            in.close();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }


        //for each album, get tostring method and save in temp arraylist
        //then populate the listview with this arraylist
        ArrayList<String> temp = new ArrayList<String>();
        temp.add("Hello");

       /*
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                temp);
        listView.setAdapter(arrayAdapter);
        */
        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
                                                Intent appInfo = new Intent(MainActivity.this, AlbumScreen.class);
                                                Bundle bundle = new Bundle();
                                                bundle.putSerializable("album", (Serializable) listView.getItemAtPosition(position));
                                                currAlbum = (Album)listView.getItemAtPosition(position);
                                                appInfo.putExtras(bundle);
                                                startActivity(appInfo);
                                            }
                                        }
        );


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        System.out.println("Yawk");
        if (v.getId() == R.id.albumlist) {
            System.out.println("Sheesh");
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle("Options");
            menu.add(Menu.NONE, 0, 0, "Rename");
            menu.add(Menu.NONE, 1, 1, "Delete");
        }
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {

        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int menuItemIndex = item.getItemId();
        if (menuItemIndex == 0) {

            //rename album
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    MainActivity.this);
            alert.setTitle("Rename");

            final EditText input = new EditText(MainActivity.this);
            alert.setView(input);

            input.setText(arrayAdapter.getItem(info.position).name);
            input.selectAll();

            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String srt1 = input.getEditableText().toString();
                    //update your listview here
                    arrayAdapter.getItem(info.position).name = srt1;
                }
            });

            alert.setNegativeButton("CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alert.create();
            alertDialog.show();
            return false;

        }
        if (menuItemIndex == 1) {
            //delete album
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm");
            builder.setMessage("Are you sure you want to delete this album?");
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Delete album
                    arrayAdapter.remove(arrayAdapter.getItem(info.position));
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
        return true;
    }

    public File bitmapToFile(Bitmap bmp, int n) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, bos);
            byte[] bArr = bos.toByteArray();
            bos.flush();
            bos.close();

            FileOutputStream fos = openFileOutput("stock"+n+".png", Context.MODE_PRIVATE);
            fos.write(bArr);
            fos.flush();
            fos.close();

            File mFile = new File(getFilesDir().getAbsolutePath(), "stock"+n+".png");
            return mFile;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
